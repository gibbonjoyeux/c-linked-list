/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_list		*lst_popend(t_list **alst) {
	t_list	*lst;
	t_list	*prev;

	if (alst == NULL || *alst == NULL)
		return NULL;
	prev = NULL;
	lst = *alst;
	// LOOP ELEMS
	while (lst->next != NULL) {
		prev = lst;
		lst = lst->next;
	}
	// FIRST == LAST
	if (prev == NULL)
		*alst = NULL;
	// FIRST != LAST
	else
		prev->next = NULL;
	return lst;
}
