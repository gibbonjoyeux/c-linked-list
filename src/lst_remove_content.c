/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		lst_remove_content(t_list **list, void *content, t_list_fdel del) {
	t_list	*cur;
	t_list	*prev;

	if (list == NULL || *list == NULL)
		return;
	cur = *list;
	prev = NULL;
	while (cur) {
		if (cur->content == content) {
			if (prev)
				prev->next = cur->next;
			else
				*list = cur->next;
			lst_del_one(&cur, del);
			return;
		}
		prev = cur;
		cur = cur->next;
	}
}
