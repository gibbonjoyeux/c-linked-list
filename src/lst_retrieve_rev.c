/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		**lst_retrieve_rev(t_list *list) {
	void	**array;
	size_t	length;

	if (list == NULL)
		return NULL;
	length = lst_len(list);
	array = (void **)calloc(length + 1, sizeof(void *));
	array[length] = NULL;
	--length;
	while (list) {
		array[length] = (void *)(list->content);
		list = list->next;
		--length;
	}
	return array;
}
