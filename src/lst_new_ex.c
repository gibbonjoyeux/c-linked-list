/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////
//
// t_list	*lst_new_ex(size_t content_size);
//
// . Returns a `t_list` which size is expended by `content_size`.
// . `content` is thus not used as a pointer but directly as memory.
// . Useful when you don't want to malloc the `content` of the `t_list` but want
//   it to be included in the `t_list` itself.
//
// WARNING:
// . Not compatible with functions using `content` as a pointer:
//  . `lst_retrieve()`
//  . `lst_retrieve_rev()`
//  . `lst_del()`			-> use `lst_del_ex()` instead
//  . `lst_del_one()`		-> use `lst_del_one_ex()` instead
//  . `lst_remove()`		-> use `lst_remove_ex()` instead
//

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_list		*lst_new_ex(size_t content_size) {
	size_t	total_size;
	t_list	*lst;

	total_size = sizeof(t_list*) + content_size;
	if (total_size < sizeof(t_list))
		total_size = sizeof(t_list);
	lst = (t_list *)malloc(total_size);
	if (lst == NULL)
		return NULL;
	lst->next = NULL;
	return lst;
}
