/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		lst_remove_ex(t_list **list, t_list *node, void (*del)(void *)) {
	t_list	*cur;
	t_list	*prev;

	if (list == NULL || *list == NULL)
		return;
	cur = *list;
	prev = NULL;
	while (cur) {
		if (cur == node) {
			if (prev)
				prev->next = cur->next;
			else
				*list = cur->next;
			lst_del_one_ex(&cur, del);
			return ;
		}
		prev = cur;
		cur = cur->next;
	}
}
