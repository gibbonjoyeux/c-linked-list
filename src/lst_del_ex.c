/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void	lst_del_ex(t_list **alst, void (*del)(void *)) {
	t_list	*next;

	if (alst == NULL || *alst == NULL)
		return;
	while (*alst) {
		next = (*alst)->next;
		lst_del_one_ex(alst, del);
		(*alst) = next;
	}
	*alst = NULL;
}
