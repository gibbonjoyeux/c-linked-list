/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_list		*lst_cindex(t_list *src_lst, ssize_t index) {
	t_list	*lst, *lst_neg;
	ssize_t	length;
	ssize_t	index_neg;

	lst = src_lst;
	length = 0;
	/// INDEX >= 0
	if (index >= 0) {
		while (lst != NULL) {
			if (length == index)
				return lst;
			length += 1;
			lst = lst->next;
		}
		index = index % length;
	/// INDEX < 0
	} else {
		lst_neg = lst;
		index_neg = index;
		while (lst != NULL) {
			if (index_neg < 0)
				index_neg += 1;
			else
				lst_neg = lst_neg->next;
			length += 1;
			lst = lst->next;
		}
		if (index_neg == 0)
			return lst_neg;
		index = length + (index % length);
	}
	/// MODULO CIRCLE
	while (lst != NULL) {
		if (index == 0)
			return lst;
		index -= 1;
		lst = lst->next;
	}
	return NULL;
}
