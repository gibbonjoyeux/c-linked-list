/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void				lst_iter(t_list **alst, char (*func)(t_list*, void*),
					void *func_param) {
	t_list			*lst;
	t_list			*lst_prev, *lst_next;

	if (alst == NULL)
		return;
	lst = *alst;
	lst_prev = NULL;
	while (lst != NULL) {
		lst_next = lst->next;
		/// DELETE ITEM
		if (func(lst, func_param) == 1) {
			if (lst_prev == NULL)
				*alst = lst_next;
			else
				lst_prev->next = lst_next;
			free(lst);
		/// KEEP ITEM
		} else {
			lst_prev = lst;
		}
		lst = lst_next;
	}
}
