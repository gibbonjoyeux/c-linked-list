/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline char	get_nodes_prev(t_list *lst, t_list *node_a, t_list *node_b,
					t_list **lst_a_prev, t_list **lst_b_prev) {
	t_list			*lst_prev;
	char			a_found, b_found;

	a_found = 0;
	b_found = 0;
	*lst_a_prev = NULL;
	*lst_b_prev = NULL;
	lst_prev = NULL;
	while (lst != NULL) {
		if (lst == node_a) {
			a_found = 1;
			*lst_a_prev = lst_prev;
		} else if (lst == node_b) {
			b_found = 1;
			*lst_b_prev = lst_prev;
		}
		lst_prev = lst;
		lst = lst->next;
	}
	return (a_found == 1 && b_found == 1);
}

static inline void	swap_nodes(t_list **alst, t_list *node_a, t_list *node_b,
					t_list *lst_a_prev, t_list *lst_b_prev) {
	t_list			*lst_tmp;

	lst_tmp = node_a->next;
	node_a->next = node_b->next;
	node_b->next = lst_tmp;
	if (lst_a_prev == NULL)
		*alst = node_b;
	else
		lst_a_prev->next = node_b;
	if (lst_b_prev == NULL)
		*alst = node_a;
	else
		lst_b_prev->next = node_a;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void				lst_swap(t_list **alst, t_list *node_a, t_list *node_b) {
	t_list			*lst_a_prev;
	t_list			*lst_b_prev;

	/// GET NODES
	if (get_nodes_prev(*alst, node_a, node_b, &lst_a_prev, &lst_b_prev) == 0)
		return;
	/// SWAP NODES
	swap_nodes(alst, node_a, node_b, lst_a_prev, lst_b_prev);
}
