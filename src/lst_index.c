/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_list			*lst_index(t_list *lst, ssize_t index) {
	t_list		*lst_neg;

	/// INDEX >= 0
	if (index >= 0) {
		while (lst != NULL) {
			if (index == 0)
				return lst;
			index -= 1;
			lst = lst->next;
		}
	/// INDEX < 0
	} else {
		lst_neg = lst;
		while (lst != NULL) {
			if (index < 0)
				index += 1;
			else
				lst_neg = lst_neg->next;
			lst = lst->next;
		}
		if (index == 0)
			return lst_neg;
	}
	return NULL;
}
