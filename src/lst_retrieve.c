/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "lst.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		**lst_retrieve(t_list *list) {
	void	**array;
	size_t	length;
	size_t	i;

	if (list == NULL)
		return NULL;
	length = lst_len(list);
	array = (void **)calloc(length + 1, sizeof(void *));
	array[length] = NULL;
	i = 0;
	while (list != NULL) {
		array[i] = (void *)(list->content);
		list = list->next;
		++i;
	}
	return array;
}
