/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef LST_H
#define LST_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>

////////////////////////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////////////////////////

typedef				struct s_list		t_list;
typedef				void				(*t_list_fdel)(void*);
typedef				char				(*t_list_fiter)(t_list*,void*);

////////////////////////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////////////////////////

struct				s_list {
	t_list			*next;
	void			*content;
};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

t_list*				lst_new(void const *content);
t_list*				lst_new_ex(size_t content_size);
void				lst_push(t_list **alst, t_list *new);
void				lst_append(t_list **alst, t_list *new);
size_t				lst_len(t_list *list);
t_list*				lst_index(t_list *list, ssize_t index);
t_list*				lst_cindex(t_list *list, ssize_t index);

void				lst_del_one(t_list **alst, t_list_fdel);
void				lst_del_one_ex(t_list **alst, t_list_fdel);
void				lst_del(t_list **alst, t_list_fdel);
void				lst_del_ex(t_list **alst, t_list_fdel);
void				lst_remove(t_list **alst, t_list *node, t_list_fdel);
void				lst_remove_ex(t_list **alst, t_list *node, t_list_fdel);
void				lst_remove_content(t_list **alst, void *content,
					t_list_fdel);
void				lst_remove_content_ex(t_list **alst, void *content,
					t_list_fdel);
void				lst_extract(t_list **alst, t_list *node);
t_list				*lst_pop(t_list **alst);
t_list				*lst_popend(t_list **alst);

void				lst_iter(t_list **alst, t_list_fiter, void *func_param);
void				lst_swap(t_list **alst, t_list *node_a, t_list *node_b);
void				lst_rev(t_list **list);
void**				lst_retrieve(t_list *list);
void**				lst_retrieve_rev(t_list *list);

#endif
